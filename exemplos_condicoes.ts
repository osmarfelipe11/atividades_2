// exemplos de condições - if else

namespace exemplos_cond 
{
    let idade: number = 11;

    if (idade >= 18){
        console.log("Pode dirigir");
    }
    else {
        console.log("Pode dirigir, mas sem parar na blits");
    }

    // Ternário
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode");
}