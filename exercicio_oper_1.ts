// Faça um programa que receba as três notas, calcule e mostre a média ponderada e o conceito da tabela

namespace exercicio_operador_1
{
    let notaLab, notaSemestre, notaExame, media: number;

    notaLab = 6;
    notaSemestre = 8;
    notaExame = 7;

    media = (notaLab * 2 + notaSemestre * 3 + notaExame * 5) / 10;

    if (media >= 8 && media <= 10){
        console.log("Sua nota é: A");
    }
    else if (media >= 7 && media < 8){
        console.log("Sua nota é: B");
    }
    else if (media >= 6 && media < 7){
        console.log("Sua nota é: C");
    }
    else if (media >= 5 && media < 6){
        console.log("Sua nota é: D");
    }
    else if (media >= 0 && media < 5){
        console.log("Sua nota é: E");
    }
}