// teste do operador E - &&

namespace operador_E
{
    let idade = 20;
    let maiorIdade = idade > 18;  // boolean (true)
    let possuiCarteiraDeMotorista = true;

    let podeDirigir = maiorIdade && possuiCarteiraDeMotorista;
    
    console.log(podeDirigir);
}