// teste operador negação - ! (inverte valor boolean/ verifica se não é verdadeiro)

namespace operador_NAO
{
    let possuiDinheiro = true;
    let estaSemDinheiro = !possuiDinheiro; 

    if (estaSemDinheiro){
        console.log("Sem dinheiro");
    }
    else {
        console.log("Tem dinheiro");
    }

    //console.log(estaSemDinheiro); // true
}