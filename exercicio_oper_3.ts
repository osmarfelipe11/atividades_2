namespace exercicio_operador_3
{
    let num, resto: number;

    num = 10;
    
    resto = num % 2;

    if (resto != 0){
        console.log(`O número ${num} é ímpar.`);
    }
    else {
        console.log(`O número ${num} é par.`);
    }

}